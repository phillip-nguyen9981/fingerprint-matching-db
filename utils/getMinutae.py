import numpy as np

def getMinutae(inhanced, original):
    inhanced = inhanced == 255
    rows, cols = original.shape
    coords = []
    for i in range(1, (rows - 1)):
        for j in range (1, (cols - 1)):
            if(inhanced[i][j]):
                block = inhanced[i-1: i+2][:,j-1: j+2]            
                neighbors = np.sum(block)
                if(neighbors == 2):
                    
                    coords.append((i, j, 1))
                elif (neighbors == 4):
                    
                    coords.append((i, j, 3))

    mask = np.zeros((rows, cols))
    for i, j, t in coords: 
        mask[i][j] = 1

    return (coords, mask)



def getMinutaeByCN(image):
    coords = []
    image = np.uint8(image == 255)
    mask = np.zeros(image.shape)
    count = 0
    for i in range(1, image.shape[0]-1):
        for j in range(1, image.shape[1]-1):
            if image[i][j] == 1:
                p = [image[i,j+1],image[i-1,j+1],image[i-1,j],image[i-1,j-1],image[i,j-1],image[i+1,j-1],image[i+1,j],image[i+1,j+1],image[i,j+1]]
                CN = 0
                # print(p)
                for k in range(len(p)-1):
                    CN += np.abs(int(p[k]) - int(p[k+1])) / 2                
                if CN == 1:
                    coords.append((i, j, 1))
                    mask[i, j] = 1
                if CN == 3:
                    coords.append((i, j, 3))
                    mask[i, j] = 1
    return coords, mask

