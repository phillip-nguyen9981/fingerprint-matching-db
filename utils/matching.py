import numpy as np
import constant


def cal_similarity(ft, fl):
    W = constant.W
    FL = np.array(fl)[3:]
    FT = np.array(ft)[3:]

    diff = np.abs(FL - FT)
    
    dot_product = W.dot(diff.T)
    if(dot_product > constant.bl):
        return 0
    
    return (np.abs(constant.bl - dot_product) / constant.bl)

def get_most_similar(feature1, feature2):
    s = 0
    x, y = None, None
    count = 0
    # print(feature1)
    # print(feature2)
    for i in range(len(feature1)):
        for j in range(len(feature2)):
            si = cal_similarity(feature1[i], feature2[j])
            if(si > s):
                s = si
                x = i
                y = j

    return x, y, s


def match_level(pv1, pv2, fv1, fv2):
    ml = np.zeros((len(pv1), len(pv2)))
    for i in range(len(pv1)):
        for j in range(len(pv2)):
            if(np.all(np.abs(pv1[i] - pv2[j]) > constant.BG)):
                continue            
            ml[i][j] = 0.5 + (0.5 * cal_similarity(fv1[i], fv2[j]))
    sum = 0
    count = 0
    while(ml.any() != 0):
        index = np.argmax(ml)
        x, y = index // len(pv2), index % len(pv2)
        if ml[x][y] != 0.5:
            sum = sum + ml[x][y]
        
        ml[x] = 0
        ml[:,y] = 0
        count = count + 1
        
    rows, cols = len(pv1), len(pv2)
    found = 0
    s = 0
    for i in range(rows):
        for j in range(cols):            
            if(ml[i, j] > 0.5):
                s += ml[i, j]
                found += 1

    return sum / count