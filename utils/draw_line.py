import numpy as np
import constant
#Bresshenham algorithm
def plotLine(p0, p1, image, angle = None, length = None):
    x0, y0 = p0[0], p0[1]
    x1, y1 = p1[0], p1[1]
    dx =  abs(x1-x0);
    coords = []
    sx = 0
    if(x0 < x1):
        sx = 1
    else:
        sx = -1
    dy = -abs(y1-y0);
    
    sy = 0;
    if(y0 < y1):
        sy = 1
    else:
        sy = -1
    
    err = dx+dy;  
    while (True):        
        coords.append((x0, y0, image[x0][y0]))
        
        if(angle and length):
            image[x0, y0] = 255

        if (x0==x1 and y0==y1): break;
        e2 = 2*err;
        if (e2 >= dy):
            err += dy; 
            x0 += sx;        
        if (e2 <= dx):
            err += dx;
            y0 += sy;
    
    if(angle and length): return

    return coords

def drawLine(arr, point, angle, length):
    startX, startY = point
    endX = np.int(startX + length*np.sin(angle))
    endY = np.int(startY + length*np.cos(angle))
    
    arr[startX, startY] = 255
    arr[endX, endY] = 255
    plotLine((startX, startY), (endX, endY), arr, angle=angle, length=length)


def drawOrientation(orien_blocks, w=16):
    rows, cols = orien_blocks.shape
    print("A")
    print(rows, cols)
    
    new_r, new_c = rows*w, cols*w
    img = np.zeros((new_r, new_c))
    for i in range(rows):
        for j in range(cols):
            block = np.zeros((w, w))
            center = (np.int(w/2), np.int(w/2))            
            drawLine(block, center, orien_blocks[i, j], w/2)
            drawLine(block, center, orien_blocks[i, j] + np.pi, w/2)
            
            img[i*w:(i+1)*w, j*w:(j+1)*w] = block

    return img