import numpy as np
from scipy import ndimage, signal

def rotatedRectWithMaxArea(image, angle):

	# https://stackoverflow.com/questions/16702966/rotate-image-and-crop-out-black-borders/16778797#16778797

	h, w = image.shape

	width_is_longer = w >= h
	side_long, side_short = (w, h) if width_is_longer else (h, w)
	
	sin_a, cos_a = abs(np.sin(angle)), abs(np.cos(angle))
	if side_short <= 2.0 * sin_a * cos_a * side_long:
		
        # Trường hợp đặc biệt nếu như điểm cắt là trung điểm

		x = 0.5 * side_short
		wr, hr = (x / sin_a, x / cos_a) if width_is_longer else (x / cos_a, x / sin_a)
	else:
		#Trường hợp tổng quát
		cos_2a = cos_a * cos_a - sin_a * sin_a
		wr, hr = (w * cos_a - h * sin_a) / cos_2a, (h * cos_a - w * sin_a) / cos_2a

	image = ndimage.interpolation.rotate(image, np.degrees(angle), reshape=False)

	hr, wr = int(hr), int(wr)
	y, x = (h - hr) // 2, (w - wr) // 2

	return image[y:y+hr, x:x+wr]


def frequest(block_img, block_orient, minWave = 5, maxWave = 15):    

    #Tính giá trị trung bình góc
    mean_angle = np.mean(block_orient)
    cos_angle = np.cos(2*mean_angle)
    sin_angle = np.sin(2*mean_angle)
    angle = np.arctan2(sin_angle, cos_angle)/2

    # Quay và cắt ảnh
    rot_img = rotatedRectWithMaxArea(block_img, angle + np.pi/2)


    #Tìm Peak và tính tần số 
    proj = np.sum(rot_img, axis=0)

    peaks = signal.find_peaks(proj)
    peaks = peaks[0]
    numPeaks = len(peaks)
    freq_block = np.zeros(block_img.shape)
    if numPeaks > 1:
        T = (peaks[-1] - peaks[0]) / (numPeaks)
        if(T > minWave and T < maxWave):
            freq_block = (1 / np.double(T)) * np.ones(block_img.shape)

    return freq_block


def frequency(img, mask, orient_img, w=42):
    rows, cols = img.shape

    freq_img = np.zeros((rows, cols))
    for i in range(0, rows, w):
        for j in range(0, cols, w):
            block_img = img[i:i+w, j:j+w];
            block_orient = orient_img[i:i+w, j:j+w]
            freq_img[i:i+w, j:j+w] = frequest(block_img, orient_img)

    freq_img = freq_img * mask


    return freq_img

