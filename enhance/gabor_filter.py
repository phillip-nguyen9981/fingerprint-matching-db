import cv2
import numpy as np
import scipy.ndimage as ndimage
def gabor_filter(norm_img, ori_img, freq_img, mask, kx=0.65, ky = 0.65):
    rows, cols = norm_img.shape
    
    # Tìm giá trị mean của frequency
    freq_1d = np.reshape(freq_img, (1, rows*cols))
    index = np.where(freq_1d > 0)
    index = index[1:]

    none_zero_elem = freq_1d[0][index]
    mean = np.mean(none_zero_elem)
    freq = mean*mask

    mean = np.double(np.round(mean*100) / 100)

    # Sinh ma gabor filter 
    # kx, ky là hệ số Gaussian theo x và y
    sigmax = 1/mean * kx
    sigmay = 1/mean * ky

    sze = np.int(np.round(3*np.max([sigmax,sigmay])));
    x,y = np.meshgrid(np.linspace(-sze,sze,(2*sze + 1)),np.linspace(-sze,sze,(2*sze + 1)));


    origin_filter = np.exp(-( ((x ** 2) / (sigmax ** 2)) + ( (y**2) / (sigmay ** 2)) / 2)) * np.cos(2*np.pi*mean*x)

    #Quay các góc của gabor và đánh index cho nó, [0] tương ứng 90 và [60] tương ứng -90 
    angleInc = 3
    angleRange = np.int(np.round(180/angleInc))
    
    # big = cv2.resize(origin_filter, (0,0), fx=5, fy=5)     
    # cv2.imshow("ori_filter", big)
    gabor_bank = np.array(np.zeros((angleRange, origin_filter.shape[0], origin_filter.shape[1])))
    for i in range(angleRange):
        angle = -(i * angleInc) + 90
        rot_filter = ndimage.rotate(origin_filter, angle, reshape=False)
        gabor_bank[i] = rot_filter
        # cv2.imshow("rot", rot_filter)
        # cv2.waitKey(0)

    # đánh chỉ số cho ảnh orient theo gabor bank ở trên, thực hiện lặp do có giá trị âm trong ảnh
    maxorientindex = np.round(180/angleInc);
    orient_index = np.round(ori_img/np.pi*maxorientindex);
    
    for i in range(0,rows):
        for j in range(0,cols):
            if(orient_index[i][j] < 1):
                orient_index[i][j] = orient_index[i][j] + maxorientindex;
            if(orient_index[i][j] > maxorientindex):
                orient_index[i][j] = orient_index[i][j] - maxorientindex;

    # thực hiện lọc
    
    # print("BEFORE")
    # print(sze)
    # print(norm_img.shape)
    
    orient_index = np.pad(orient_index, sze, mode="edge")
    tmp_img = np.pad(norm_img, sze, mode="edge")
    tmp_freq = np.pad(freq, sze, mode="edge")
    newim = np.zeros(tmp_img.shape);
    
    # print("Padded")
    # print(newim.shape)

    for i in range(sze, rows - sze):
        for j in range(sze, cols - sze):
            if(tmp_freq[i, j] > 0):
                
                block = tmp_img[i-sze: i+sze+1, j-sze: j+sze+1]
                newim[i][j] = np.sum(block * gabor_bank[int(orient_index[i, j]) - 1]);
    newim = newim[sze: rows-sze, sze: cols-sze]
    
    # print("After, ", newim.shape)
    return(newim);    